package com.example.cloudspringbootcustomerservice.biz;

import com.example.cloudspringbootcustomerservice.pojo.WorkOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工作订单 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface IWorkOrderService extends IService<WorkOrder> {

}
