package com.example.cloudspringbootcustomerservice.biz;

import com.example.cloudspringbootcustomerservice.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface IUserService extends IService<User> {

}
