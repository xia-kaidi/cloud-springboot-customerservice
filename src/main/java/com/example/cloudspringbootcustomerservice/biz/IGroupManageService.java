package com.example.cloudspringbootcustomerservice.biz;

import com.example.cloudspringbootcustomerservice.pojo.GroupManage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 组管理 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface IGroupManageService extends IService<GroupManage> {

}
