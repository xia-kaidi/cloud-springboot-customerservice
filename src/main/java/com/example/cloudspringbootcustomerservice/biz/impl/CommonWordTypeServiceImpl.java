package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.CommonWordType;
import com.example.cloudspringbootcustomerservice.dao.CommonWordTypeMapper;
import com.example.cloudspringbootcustomerservice.biz.ICommonWordTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 常见的用语—类型 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class CommonWordTypeServiceImpl extends ServiceImpl<CommonWordTypeMapper, CommonWordType> implements ICommonWordTypeService {

}
