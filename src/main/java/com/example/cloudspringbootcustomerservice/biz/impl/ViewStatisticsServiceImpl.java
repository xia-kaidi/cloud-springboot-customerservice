package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.ViewStatistics;
import com.example.cloudspringbootcustomerservice.dao.ViewStatisticsMapper;
import com.example.cloudspringbootcustomerservice.biz.IViewStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 视图统计 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class ViewStatisticsServiceImpl extends ServiceImpl<ViewStatisticsMapper, ViewStatistics> implements IViewStatisticsService {

}
