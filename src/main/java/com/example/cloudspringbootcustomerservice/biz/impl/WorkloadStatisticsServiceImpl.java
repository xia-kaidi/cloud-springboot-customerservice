package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.WorkloadStatistics;
import com.example.cloudspringbootcustomerservice.dao.WorkloadStatisticsMapper;
import com.example.cloudspringbootcustomerservice.biz.IWorkloadStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工作量统计 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class WorkloadStatisticsServiceImpl extends ServiceImpl<WorkloadStatisticsMapper, WorkloadStatistics> implements IWorkloadStatisticsService {

}
