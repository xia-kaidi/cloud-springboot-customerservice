package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.Tags;
import com.example.cloudspringbootcustomerservice.dao.TagsMapper;
import com.example.cloudspringbootcustomerservice.biz.ITagsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标签 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class TagsServiceImpl extends ServiceImpl<TagsMapper, Tags> implements ITagsService {

}
