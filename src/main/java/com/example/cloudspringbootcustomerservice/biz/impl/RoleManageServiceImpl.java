package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.RoleManage;
import com.example.cloudspringbootcustomerservice.dao.RoleManageMapper;
import com.example.cloudspringbootcustomerservice.biz.IRoleManageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色管理 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class RoleManageServiceImpl extends ServiceImpl<RoleManageMapper, RoleManage> implements IRoleManageService {

}
