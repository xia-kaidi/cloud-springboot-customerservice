package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.WorkOrderReply;
import com.example.cloudspringbootcustomerservice.dao.WorkOrderReplyMapper;
import com.example.cloudspringbootcustomerservice.biz.IWorkOrderReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工作订单回复 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class WorkOrderReplyServiceImpl extends ServiceImpl<WorkOrderReplyMapper, WorkOrderReply> implements IWorkOrderReplyService {

}
