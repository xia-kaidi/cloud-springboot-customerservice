package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.GroupManage;
import com.example.cloudspringbootcustomerservice.dao.GroupManageMapper;
import com.example.cloudspringbootcustomerservice.biz.IGroupManageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组管理 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class GroupManageServiceImpl extends ServiceImpl<GroupManageMapper, GroupManage> implements IGroupManageService {

}
