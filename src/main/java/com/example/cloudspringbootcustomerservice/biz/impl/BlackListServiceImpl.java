package com.example.cloudspringbootcustomerservice.biz.impl;

import com.example.cloudspringbootcustomerservice.pojo.BlackList;
import com.example.cloudspringbootcustomerservice.dao.BlackListMapper;
import com.example.cloudspringbootcustomerservice.biz.IBlackListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 恶意用户列表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class BlackListServiceImpl extends ServiceImpl<BlackListMapper, BlackList> implements IBlackListService {

}
