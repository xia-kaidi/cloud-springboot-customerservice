package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.WorkQualityStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 工作质量统计 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface WorkQualityStatisticsMapper extends BaseMapper<WorkQualityStatistics> {

}
