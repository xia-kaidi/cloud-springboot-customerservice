package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.SessionMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会话管理 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface SessionMessageMapper extends BaseMapper<SessionMessage> {

}
