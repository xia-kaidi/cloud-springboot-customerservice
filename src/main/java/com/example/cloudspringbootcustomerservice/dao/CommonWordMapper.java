package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.CommonWord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 常见的用语 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface CommonWordMapper extends BaseMapper<CommonWord> {

}
