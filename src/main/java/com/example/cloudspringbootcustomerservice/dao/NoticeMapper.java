package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 通知表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
