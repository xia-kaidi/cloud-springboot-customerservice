package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.RoleManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色管理 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface RoleManageMapper extends BaseMapper<RoleManage> {

}
