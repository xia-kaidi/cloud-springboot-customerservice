package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.BlackList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 恶意用户列表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface BlackListMapper extends BaseMapper<BlackList> {

}
