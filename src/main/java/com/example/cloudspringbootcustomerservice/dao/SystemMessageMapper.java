package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.SystemMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统信息 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
// new test
public interface SystemMessageMapper extends BaseMapper<SystemMessage> {

}
