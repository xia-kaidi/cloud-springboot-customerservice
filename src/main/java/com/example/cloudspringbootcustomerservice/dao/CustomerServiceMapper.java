package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.CustomerService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客服表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface CustomerServiceMapper extends BaseMapper<CustomerService> {

}
