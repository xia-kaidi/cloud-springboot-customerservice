package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.Session;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会话表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface SessionMapper extends BaseMapper<Session> {

}
