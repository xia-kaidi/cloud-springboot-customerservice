package com.example.cloudspringbootcustomerservice.dao;

import com.example.cloudspringbootcustomerservice.pojo.WorkLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 工作日志 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface WorkLogMapper extends BaseMapper<WorkLog> {

}
