package com.example.cloudspringbootcustomerservice;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j

public class MyMeta0bjectHandler  implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("====>start insert fill.....");
        this.setFieldValByName("updatetime",new Date(),metaObject);
        this.setFieldValByName("createtime",new Date(),metaObject);
    }
    //更新时填充
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("====>start update fill.....");
        this.setFieldValByName("updatetime",new Date(),metaObject);
    }

}
