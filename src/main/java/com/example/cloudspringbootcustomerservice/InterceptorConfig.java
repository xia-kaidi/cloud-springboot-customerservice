package com.example.cloudspringbootcustomerservice;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new Filter()).addPathPatterns("/**").excludePathPatterns("/Register/WantToCreate","/assets/**","/logo/**","/login","/customerService/login","/","/register.html","/customerService/Setting");
    }
}
