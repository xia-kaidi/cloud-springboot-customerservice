package com.example.cloudspringbootcustomerservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.cloudspringbootcustomerservice.biz.ICustomerInfoService;
import com.example.cloudspringbootcustomerservice.biz.ICustomerServiceService;
import com.example.cloudspringbootcustomerservice.biz.IWorkOrderService;
import com.example.cloudspringbootcustomerservice.pojo.CustomerInfo;
import com.example.cloudspringbootcustomerservice.pojo.CustomerService;
import com.example.cloudspringbootcustomerservice.pojo.WorkOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 工作订单 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
public class WorkOrderController {
    @Autowired
    private IWorkOrderService iWorkOrderService;
    @Autowired
    private ICustomerInfoService iCustomerInfoService;
    @Autowired
    private ICustomerServiceService iCustomerServiceService;

    @RequestMapping("/transmit")
    public String ViewCustomerService (Integer id,Model model, WorkOrder workOrder, @RequestParam("current")Integer current,@RequestParam("size") Integer size,HttpSession session,HttpServletResponse response){
        session.setAttribute("current",current);
        session.setAttribute("size",size);
        CustomerService cs=(CustomerService) session.getAttribute("user");
        IPage<CustomerService> page=new Page<>(current,size);
        IPage<CustomerService> page1 = iCustomerServiceService.page(page,new QueryWrapper<CustomerService>().eq("state","online").ne("customerserviceid",cs.getCustomerserviceid()));
        List<CustomerService> list = page1.getRecords();
//        System.out.println(list);
//        System.out.println(list.size());
        long pagesCount = page1.getPages();
        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);
        session.setAttribute("msg","在线客服列表");
        session.setAttribute("workorderid",workOrder.getId());
        return "CustomerServiceList";
    }




    @RequestMapping("/workorderlist")
    public String initWorkOrder(Model model, Integer current, Integer size, HttpSession session)
    {

        session.setAttribute("current",current);
        session.setAttribute("size",size);
        IPage<WorkOrder> page = new Page<>(current,size);
        Integer id=((CustomerService)session.getAttribute("user")).getCustomerserviceid();
        IPage<WorkOrder> page1 = this.iWorkOrderService.page(page,new QueryWrapper<WorkOrder>().eq("customerServiceId",id));


        List<WorkOrder> list = page1.getRecords();
        long pagesCount = page1.getPages();

        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);
//        List<WorkOrder> list = iWorkOrderService.list(new QueryWrapper<WorkOrder>().eq("customerserviceid",id));
//        model.addAttribute("list",list);
        session.setAttribute("msg","工单列表");
        return "workOrder";
    }
    @RequestMapping("/del")
    public String delete(HttpSession session,Model model, WorkOrder workOrder, @RequestParam("current")Integer current, @RequestParam("size") Integer size,HttpServletResponse response) throws IOException {
        session.setAttribute("current",current);
        session.setAttribute("size",size);
        this.iWorkOrderService.removeById(workOrder.getId());
        List<WorkOrder> list = iWorkOrderService.list();
        Flash(session);
        model.addAttribute("list",list);
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String ans="?current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="\"<script>alert('删除');window.location='/workorderlist";
        String s2="';</script>\"";
        out.print(s1+ans+s2);
        return null;
    }

    @RequestMapping("/details")
    public String details(Model model,WorkOrder workOrder)
    {
        // 根据id将对像查询
        workOrder= this.iWorkOrderService.getById(workOrder.getId());
        model.addAttribute("wo",workOrder);
        return  "details";
    }
    private void Flash(HttpSession session) {
        CustomerService cs=(CustomerService)session.getAttribute("user");
        session.setAttribute("Hurry", this.iWorkOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "紧急").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("High", this.iWorkOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "高").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Medium", this.iWorkOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "中").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Low", this.iWorkOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "低").eq("customerServiceId", cs.getCustomerserviceid())));
    }
    @RequestMapping("/create")
    public String create(Integer id,HttpSession session,Integer current,Integer size) {
        session.setAttribute("current",current);
        session.setAttribute("size",size);
        CustomerInfo cif=iCustomerInfoService.getById(id);
        session.setAttribute("customer",cif);
        return "createworkorder";
    }
    @RequestMapping(value = "/SubmitWorkOrdel",method = RequestMethod.POST)
    String WorkOrdersave(WorkOrder wo,HttpSession session,Model model,HttpServletResponse response) throws IOException {

        Flash(session);
        CustomerInfo cif=(CustomerInfo) session.getAttribute("customer");
        CustomerService cs=(CustomerService)session.getAttribute("user");
        wo.setCustomerid(cif.getId());
        wo.setCustomerserviceid(cs.getCustomerserviceid());
        iWorkOrderService.save(wo);
        System.out.println("-------------------"+wo);
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String ans="/?current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="\"<script>alert('添加成功');window.location='/CustomerList";
        String s2="';</script>\"";
        out.print(s1+ans+s2);
        Flash(session);
        return null;
    }
    @RequestMapping("/update")
    public String update(Model model, WorkOrder workOrder, @RequestParam("current")Integer current,@RequestParam("size") Integer size)
    {
        // 根据id将对像查询
        workOrder= this.iWorkOrderService.getById(workOrder.getId());
        model.addAttribute("wo",workOrder);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
        //model.addAttribute("main","客服修改");
        return  "update";
    }
    @PostMapping("/updates")
    public String updates(Model model, WorkOrder workOrder, HttpSession session, HttpServletResponse response) throws IOException {
        System.out.println("---------------"+workOrder);
        System.out.println(iWorkOrderService.updateById(workOrder));
        Flash(session);
        String ans="current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="redirect:/workorderlist?";
        return s1+ans;
    }
}

