package com.example.cloudspringbootcustomerservice.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 客服表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@RestController
@RequestMapping("/customerService")
public class CustomerServiceController {

}

