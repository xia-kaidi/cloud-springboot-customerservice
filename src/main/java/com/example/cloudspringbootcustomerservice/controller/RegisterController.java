package com.example.cloudspringbootcustomerservice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.cloudspringbootcustomerservice.biz.ICustomerServiceService;
import com.example.cloudspringbootcustomerservice.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class RegisterController {

    @Autowired
    ICustomerServiceService iss;
    @RequestMapping("/register")
    public String wqe(){
        return "register";
    }
    @RequestMapping("/WantToCreate")
    public String weqweqw(CustomerService ss, HttpSession session){
        String ans="";
       int t=iss.count(new QueryWrapper<CustomerService>().eq("email",ss.getEmail()));
        if(t!=0){        //如果之前就存在账户,不创建
            session.setAttribute("message","账户已经存在");
            ans="forward:/register";
        }
        else{
            iss.save(ss);
            ans="index";
        }
        return ans;
    }
}
