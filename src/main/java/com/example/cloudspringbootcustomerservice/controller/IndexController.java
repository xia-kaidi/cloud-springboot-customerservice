package com.example.cloudspringbootcustomerservice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.cloudspringbootcustomerservice.biz.ICustomerServiceService;
import com.example.cloudspringbootcustomerservice.biz.INoticeService;
import com.example.cloudspringbootcustomerservice.biz.IWorkOrderService;
import com.example.cloudspringbootcustomerservice.pojo.CustomerService;

import com.example.cloudspringbootcustomerservice.pojo.Notice;
import com.example.cloudspringbootcustomerservice.pojo.WorkOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

@Controller
public class IndexController {
    @Autowired
    ICustomerServiceService iCustomerServiceService;
    @Autowired
    IWorkOrderService workOrderService;
    @Autowired
    INoticeService noticeService;
    @RequestMapping("")
     String ff(){
        return "login";
    }
    @RequestMapping("/login")
    public String qw(CustomerService customerService, Model model, HttpSession session, HttpServletRequest httpServletRequest, HttpServletResponse response) {

        String ans="";
//        CustomerService cs=customerService;
        CustomerService cs = iCustomerServiceService.getOne
                (new QueryWrapper<CustomerService>().
                        eq("email", customerService.getEmail()).
                        eq("password", customerService.getPassword()));//获取符合条件的一条完整记录
        if (cs == null) {//密码错误
            session.setAttribute("loginMessage", "账号密码错误");
            ans = "login";
        } else {//请到这边来
            session.setAttribute("msg","首页");
            //  获取该客服各类工单开始
            //  获取该客服各类工单结束

            // 个人信息界面读入开始
            int SoupCount = this.noticeService.count();
            int randomId = (int) (Math.random() * SoupCount) + 1;
            Notice notice = noticeService.getOne
                    (new QueryWrapper<Notice>().
                            eq("id", randomId));
            session.setAttribute("Name", cs.getNickname());
            session.setAttribute("ServiceId", cs.getCustomerserviceid());
            session.setAttribute("Soup", notice.getContent());
            session.setAttribute("Expression", notice.getTitle());
            //  个人信息界面读入结束

            ans = "forward:/index";

            //  未知代码删了却报错开始
            session.setAttribute("record", "true");
            session.setAttribute("user", cs);
            UpdateWrapper<CustomerService> uwp=new UpdateWrapper<CustomerService>();
            uwp.set("state","online");
            uwp.eq("customerServiceId",cs.getCustomerserviceid());
            iCustomerServiceService.update(cs,uwp);
            Flash(session);
            //  未知代码删了却报错结束
        }
        return ans;
    }

    private void Flash(HttpSession session) {
        CustomerService cs=(CustomerService)session.getAttribute("user");
        session.setAttribute("Hurry", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "紧急").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("High", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "高").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Medium", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "中").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Low", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "低").eq("customerServiceId", cs.getCustomerserviceid())));
    }

    @RequestMapping("/Setting")
    public String Setting(Model model, HttpSession session) {
        CustomerService cs = (CustomerService) session.getAttribute("user");
        //  获取需要修改的初始信息开始
        model.addAttribute("LastName", cs.getRealname());
        model.addAttribute("LastNickName", cs.getNickname());
        model.addAttribute("LastPhone", cs.getCustomerservicephone());
        model.addAttribute("LastGroup", cs.getServicegroup());
        model.addAttribute("LastRole", cs.getServicerole());
        //  获取需要修改的初始信息结束
        model.addAttribute("msg","个人信息");
        return "layouts";      //  跳转信息设置页面
    }
    @RequestMapping("/addForm")
    public String add(CustomerService customerService, Model model, HttpSession session,HttpServletResponse response) throws IOException {
//        System.out.println(session.getAttribute("email"));
        UpdateWrapper<CustomerService> updateWrapper = new UpdateWrapper<>();
        CustomerService cs=(CustomerService)session.getAttribute("user");
        updateWrapper.eq("email", cs.getEmail());
        iCustomerServiceService.update(customerService, updateWrapper);  // 更新

        cs=iCustomerServiceService.getOne(updateWrapper);
        session.setAttribute("user",cs);
        session.setAttribute("Name", cs.getNickname());
        session.setAttribute("ServiceId", cs.getCustomerserviceid());

        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out=response.getWriter();
        out.print("<script>alert('修改成功');window.location='/index';</script>");
        return null;
    }
        @RequestMapping("/index")
        public String w2qe(HttpSession session){

            session.setAttribute("msg","首页");
            return "index";
    }

        @RequestMapping("/NewCustomer")
         public String srqerq(){
        return "NewCustomer";
        }
        @RequestMapping("/transmitTo")
        public String transmit(Integer id, @RequestParam("current")Integer current, @RequestParam("size") Integer size, HttpSession session, HttpServletResponse response) throws IOException {
        WorkOrder Wo=workOrderService.getById((Integer)session.getAttribute("workorderid"));
        int onlineCustomerServiceCount=iCustomerServiceService.count(new QueryWrapper<CustomerService>().eq("state","online"));
        UpdateWrapper<WorkOrder> updateWrapper=new UpdateWrapper<>();
        updateWrapper.eq("id",Wo.getId());
        updateWrapper.set("customerServiceId",null);
        workOrderService.update(Wo,updateWrapper);
            UpdateWrapper<WorkOrder> updateWrapper1=new UpdateWrapper<>();
            updateWrapper1.eq("id",Wo.getId());
            updateWrapper1.set("customerServiceId",id);
            workOrderService.update(Wo,updateWrapper1);
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String ans="?current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="\"<script>alert('转发');window.location='/workorderlist";
        String s2="';</script>\"";
        out.print(s1+ans+s2);
        return null;
    }

}

