package com.example.cloudspringbootcustomerservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.cloudspringbootcustomerservice.biz.ICustomerInfoService;
import com.example.cloudspringbootcustomerservice.pojo.CustomerInfo;
import com.example.cloudspringbootcustomerservice.pojo.CustomerService;
import com.example.cloudspringbootcustomerservice.pojo.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * <p>
 * 客户信息 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
public class CustomerInfoController {

    @Autowired
    ICustomerInfoService iis;
    @RequestMapping("/CustomerList")
    public String index(Model model, HttpSession session,Integer current,Integer size){

        session.setAttribute("current",current);
        session.setAttribute("size",size);
        Integer id=((CustomerService)session.getAttribute("user")).getCustomerserviceid();
        IPage<CustomerInfo> page=new Page<>(current,size);
        IPage<CustomerInfo> page1 = iis.page(page,new QueryWrapper<CustomerInfo>().eq("customerServiceId",id));
        List<CustomerInfo> list = page1.getRecords();
//        System.out.println(list);
//        System.out.println(list.size());
        long pagesCount = page1.getPages();
        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);
       session.setAttribute("msg","客户列表");

        return "customerinfo";

    }
    @RequestMapping("/submitCustomer")
    public String ewgfhg2y1u(HttpSession session,CustomerInfo customerInfo, HttpServletResponse response) throws IOException {
        int count=iis.count(new QueryWrapper<CustomerInfo>().eq("email",customerInfo.getEmail()));
        customerInfo.setCustomerserviceid(((CustomerService)session.getAttribute("user")).getCustomerserviceid());
        if(count==0){   //之前没有
            iis.save(customerInfo);
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");

            PrintWriter out=response.getWriter();
            out.print("<script>alert('创建成功');window.location='/CustomerList/?current=1&size=5';</script>");
        }
        else{           //之前有
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");

            PrintWriter out=response.getWriter();
            out.print("<script>alert('用户已经存在');window.location='/NewCustomer';</script>");

        }
        return null;
    }
    @RequestMapping("/updateinfo")
    String UpDate(Integer id,Model model){
        //根据ID找到数据库中的对象
        CustomerInfo ci=iis.getById(id);
        model.addAttribute("realname",ci.getRealname());
        model.addAttribute("phone",ci.getPhone());
        model.addAttribute("address",ci.getAddress());
        model.addAttribute("email",ci.getEmail());
        model.addAttribute("level",ci.getLevel());
        model.addAttribute("remark",ci.getRemark());
        model.addAttribute("id",ci.getId());
        return "UpdateCustomer";

    }
    @RequestMapping("/delete")
    String Delete(HttpSession session,Integer id,HttpServletResponse response) throws IOException {
        iis.removeById(id);
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String ans="/?current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="\"<script>alert('删除');window.location='/CustomerList";
        String s2="';</script>\"";
        out.print(s1+ans+s2);
        return null;
    }

    @RequestMapping("/updateCustomer")
    String UpdateCustomer(HttpSession session,CustomerInfo customerInfo,HttpServletResponse response) throws IOException {
        System.out.println(customerInfo);
        iis.saveOrUpdate(customerInfo);
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        String ans="/?current="+(Integer)session.getAttribute("current");
        ans+="&size="+(Integer)session.getAttribute("size");
        PrintWriter out=response.getWriter();
        String s1="\"<script>alert('更改');window.location='/CustomerList";
        String s2="';</script>\"";
        out.print(s1+ans+s2);

        return null;
    }

    @RequestMapping("/viewcustomerinfo")
    String viewCustomer(Integer id,Model model){
        CustomerInfo cif=iis.getById(id);
        model.addAttribute("info",cif);
        return "cif";
    }

}

