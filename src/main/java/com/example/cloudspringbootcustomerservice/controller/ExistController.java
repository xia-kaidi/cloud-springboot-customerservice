package com.example.cloudspringbootcustomerservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class ExistController {
    //退出
    @RequestMapping("/exist")
    public String wqeqwe(HttpSession session){
        session.invalidate();
        return "redirect:/";
    }
}
