package com.example.cloudspringbootcustomerservice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.cloudspringbootcustomerservice.biz.ICustomerInfoService;
import com.example.cloudspringbootcustomerservice.biz.ICustomerServiceService;
import com.example.cloudspringbootcustomerservice.biz.INoticeService;
import com.example.cloudspringbootcustomerservice.biz.IWorkOrderService;
import com.example.cloudspringbootcustomerservice.pojo.CustomerService;
import com.example.cloudspringbootcustomerservice.pojo.WorkOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.servlet.http.HttpSession;

@RequestMapping ("mouichidocontroller")
@Controller
public class MouichidoController {
    @Autowired
    private ICustomerServiceService iCustomerServiceService;
    @Autowired
    private ICustomerInfoService customerInfoService;
    @Autowired
    private INoticeService noticeService;
    @Autowired
    private IWorkOrderService workOrderService;
    @RequestMapping("register")
    public String register(){

        return "register";
    }

    @RequestMapping("login")
    public String index(CustomerService customerService, Model model, HttpSession session) {

        CustomerService cs = iCustomerServiceService.getOne
                (new QueryWrapper<CustomerService>().
                        eq("email", session.getAttribute("email")));
        //  获取该客服各类工单开始

        session.setAttribute("Hurry", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "紧急").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("High", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "高").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Medium", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "中").eq("customerServiceId", cs.getCustomerserviceid())));
        session.setAttribute("Low", this.workOrderService.count(new QueryWrapper<WorkOrder>().eq("priority", "低").eq("customerServiceId", cs.getCustomerserviceid())));
        // 获取该客服各类工单结束

        // 个人信息界面读入开始
        session.setAttribute("Name", cs.getNickname());
        session.setAttribute("ServiceId", cs.getCustomerserviceid());
        //  个人信息界面读入结束

        return "redirect:/index";
    }
}
